#ifndef create_transfer_request_TEST
#define create_transfer_request_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define create_transfer_request_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/create_transfer_request.h"
create_transfer_request_t* instantiate_create_transfer_request(int include_optional);



create_transfer_request_t* instantiate_create_transfer_request(int include_optional) {
  create_transfer_request_t* create_transfer_request = NULL;
  if (include_optional) {
    create_transfer_request = create_transfer_request_create(
      "0",
      56,
      mobwa_payments_hub_create_transfer_request_CURRENCY_SGD,
      1,
      "0"
    );
  } else {
    create_transfer_request = create_transfer_request_create(
      "0",
      56,
      mobwa_payments_hub_create_transfer_request_CURRENCY_SGD,
      1,
      "0"
    );
  }

  return create_transfer_request;
}


#ifdef create_transfer_request_MAIN

void test_create_transfer_request(int include_optional) {
    create_transfer_request_t* create_transfer_request_1 = instantiate_create_transfer_request(include_optional);

	cJSON* jsoncreate_transfer_request_1 = create_transfer_request_convertToJSON(create_transfer_request_1);
	printf("create_transfer_request :\n%s\n", cJSON_Print(jsoncreate_transfer_request_1));
	create_transfer_request_t* create_transfer_request_2 = create_transfer_request_parseFromJSON(jsoncreate_transfer_request_1);
	cJSON* jsoncreate_transfer_request_2 = create_transfer_request_convertToJSON(create_transfer_request_2);
	printf("repeating create_transfer_request:\n%s\n", cJSON_Print(jsoncreate_transfer_request_2));
}

int main() {
  test_create_transfer_request(1);
  test_create_transfer_request(0);

  printf("Hello world \n");
  return 0;
}

#endif // create_transfer_request_MAIN
#endif // create_transfer_request_TEST
