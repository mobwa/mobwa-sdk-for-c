#ifndef sign_up_request_TEST
#define sign_up_request_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define sign_up_request_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/sign_up_request.h"
sign_up_request_t* instantiate_sign_up_request(int include_optional);



sign_up_request_t* instantiate_sign_up_request(int include_optional) {
  sign_up_request_t* sign_up_request = NULL;
  if (include_optional) {
    sign_up_request = sign_up_request_create(
      "0",
      "0",
      "0"
    );
  } else {
    sign_up_request = sign_up_request_create(
      "0",
      "0",
      "0"
    );
  }

  return sign_up_request;
}


#ifdef sign_up_request_MAIN

void test_sign_up_request(int include_optional) {
    sign_up_request_t* sign_up_request_1 = instantiate_sign_up_request(include_optional);

	cJSON* jsonsign_up_request_1 = sign_up_request_convertToJSON(sign_up_request_1);
	printf("sign_up_request :\n%s\n", cJSON_Print(jsonsign_up_request_1));
	sign_up_request_t* sign_up_request_2 = sign_up_request_parseFromJSON(jsonsign_up_request_1);
	cJSON* jsonsign_up_request_2 = sign_up_request_convertToJSON(sign_up_request_2);
	printf("repeating sign_up_request:\n%s\n", cJSON_Print(jsonsign_up_request_2));
}

int main() {
  test_sign_up_request(1);
  test_sign_up_request(0);

  printf("Hello world \n");
  return 0;
}

#endif // sign_up_request_MAIN
#endif // sign_up_request_TEST
