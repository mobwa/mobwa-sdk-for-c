#ifndef transfer_source_TEST
#define transfer_source_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define transfer_source_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/transfer_source.h"
transfer_source_t* instantiate_transfer_source(int include_optional);



transfer_source_t* instantiate_transfer_source(int include_optional) {
  transfer_source_t* transfer_source = NULL;
  if (include_optional) {
    transfer_source = transfer_source_create(
      "0"
    );
  } else {
    transfer_source = transfer_source_create(
      "0"
    );
  }

  return transfer_source;
}


#ifdef transfer_source_MAIN

void test_transfer_source(int include_optional) {
    transfer_source_t* transfer_source_1 = instantiate_transfer_source(include_optional);

	cJSON* jsontransfer_source_1 = transfer_source_convertToJSON(transfer_source_1);
	printf("transfer_source :\n%s\n", cJSON_Print(jsontransfer_source_1));
	transfer_source_t* transfer_source_2 = transfer_source_parseFromJSON(jsontransfer_source_1);
	cJSON* jsontransfer_source_2 = transfer_source_convertToJSON(transfer_source_2);
	printf("repeating transfer_source:\n%s\n", cJSON_Print(jsontransfer_source_2));
}

int main() {
  test_transfer_source(1);
  test_transfer_source(0);

  printf("Hello world \n");
  return 0;
}

#endif // transfer_source_MAIN
#endif // transfer_source_TEST
