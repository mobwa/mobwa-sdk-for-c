#ifndef recharge_account_request_TEST
#define recharge_account_request_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define recharge_account_request_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/recharge_account_request.h"
recharge_account_request_t* instantiate_recharge_account_request(int include_optional);



recharge_account_request_t* instantiate_recharge_account_request(int include_optional) {
  recharge_account_request_t* recharge_account_request = NULL;
  if (include_optional) {
    recharge_account_request = recharge_account_request_create(
      56
    );
  } else {
    recharge_account_request = recharge_account_request_create(
      56
    );
  }

  return recharge_account_request;
}


#ifdef recharge_account_request_MAIN

void test_recharge_account_request(int include_optional) {
    recharge_account_request_t* recharge_account_request_1 = instantiate_recharge_account_request(include_optional);

	cJSON* jsonrecharge_account_request_1 = recharge_account_request_convertToJSON(recharge_account_request_1);
	printf("recharge_account_request :\n%s\n", cJSON_Print(jsonrecharge_account_request_1));
	recharge_account_request_t* recharge_account_request_2 = recharge_account_request_parseFromJSON(jsonrecharge_account_request_1);
	cJSON* jsonrecharge_account_request_2 = recharge_account_request_convertToJSON(recharge_account_request_2);
	printf("repeating recharge_account_request:\n%s\n", cJSON_Print(jsonrecharge_account_request_2));
}

int main() {
  test_recharge_account_request(1);
  test_recharge_account_request(0);

  printf("Hello world \n");
  return 0;
}

#endif // recharge_account_request_MAIN
#endif // recharge_account_request_TEST
