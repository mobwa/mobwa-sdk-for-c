#ifndef update_transfer_request_TEST
#define update_transfer_request_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define update_transfer_request_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/update_transfer_request.h"
update_transfer_request_t* instantiate_update_transfer_request(int include_optional);



update_transfer_request_t* instantiate_update_transfer_request(int include_optional) {
  update_transfer_request_t* update_transfer_request = NULL;
  if (include_optional) {
    update_transfer_request = update_transfer_request_create(
      "0",
      56
    );
  } else {
    update_transfer_request = update_transfer_request_create(
      "0",
      56
    );
  }

  return update_transfer_request;
}


#ifdef update_transfer_request_MAIN

void test_update_transfer_request(int include_optional) {
    update_transfer_request_t* update_transfer_request_1 = instantiate_update_transfer_request(include_optional);

	cJSON* jsonupdate_transfer_request_1 = update_transfer_request_convertToJSON(update_transfer_request_1);
	printf("update_transfer_request :\n%s\n", cJSON_Print(jsonupdate_transfer_request_1));
	update_transfer_request_t* update_transfer_request_2 = update_transfer_request_parseFromJSON(jsonupdate_transfer_request_1);
	cJSON* jsonupdate_transfer_request_2 = update_transfer_request_convertToJSON(update_transfer_request_2);
	printf("repeating update_transfer_request:\n%s\n", cJSON_Print(jsonupdate_transfer_request_2));
}

int main() {
  test_update_transfer_request(1);
  test_update_transfer_request(0);

  printf("Hello world \n");
  return 0;
}

#endif // update_transfer_request_MAIN
#endif // update_transfer_request_TEST
