#ifndef create_account_request_TEST
#define create_account_request_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define create_account_request_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/create_account_request.h"
create_account_request_t* instantiate_create_account_request(int include_optional);



create_account_request_t* instantiate_create_account_request(int include_optional) {
  create_account_request_t* create_account_request = NULL;
  if (include_optional) {
    create_account_request = create_account_request_create(
      "0"
    );
  } else {
    create_account_request = create_account_request_create(
      "0"
    );
  }

  return create_account_request;
}


#ifdef create_account_request_MAIN

void test_create_account_request(int include_optional) {
    create_account_request_t* create_account_request_1 = instantiate_create_account_request(include_optional);

	cJSON* jsoncreate_account_request_1 = create_account_request_convertToJSON(create_account_request_1);
	printf("create_account_request :\n%s\n", cJSON_Print(jsoncreate_account_request_1));
	create_account_request_t* create_account_request_2 = create_account_request_parseFromJSON(jsoncreate_account_request_1);
	cJSON* jsoncreate_account_request_2 = create_account_request_convertToJSON(create_account_request_2);
	printf("repeating create_account_request:\n%s\n", cJSON_Print(jsoncreate_account_request_2));
}

int main() {
  test_create_account_request(1);
  test_create_account_request(0);

  printf("Hello world \n");
  return 0;
}

#endif // create_account_request_MAIN
#endif // create_account_request_TEST
