#ifndef sign_up_response_TEST
#define sign_up_response_TEST

// the following is to include only the main from the first c file
#ifndef TEST_MAIN
#define TEST_MAIN
#define sign_up_response_MAIN
#endif // TEST_MAIN

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "../external/cJSON.h"

#include "../model/sign_up_response.h"
sign_up_response_t* instantiate_sign_up_response(int include_optional);



sign_up_response_t* instantiate_sign_up_response(int include_optional) {
  sign_up_response_t* sign_up_response = NULL;
  if (include_optional) {
    sign_up_response = sign_up_response_create(
      "0",
      "0",
      mobwa_payments_hub_sign_up_response_ROLE_customer,
      list_create()
    );
  } else {
    sign_up_response = sign_up_response_create(
      "0",
      "0",
      mobwa_payments_hub_sign_up_response_ROLE_customer,
      list_create()
    );
  }

  return sign_up_response;
}


#ifdef sign_up_response_MAIN

void test_sign_up_response(int include_optional) {
    sign_up_response_t* sign_up_response_1 = instantiate_sign_up_response(include_optional);

	cJSON* jsonsign_up_response_1 = sign_up_response_convertToJSON(sign_up_response_1);
	printf("sign_up_response :\n%s\n", cJSON_Print(jsonsign_up_response_1));
	sign_up_response_t* sign_up_response_2 = sign_up_response_parseFromJSON(jsonsign_up_response_1);
	cJSON* jsonsign_up_response_2 = sign_up_response_convertToJSON(sign_up_response_2);
	printf("repeating sign_up_response:\n%s\n", cJSON_Print(jsonsign_up_response_2));
}

int main() {
  test_sign_up_response(1);
  test_sign_up_response(0);

  printf("Hello world \n");
  return 0;
}

#endif // sign_up_response_MAIN
#endif // sign_up_response_TEST
