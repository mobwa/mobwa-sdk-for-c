# create_transfer_request_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **char \*** |  | [optional] 
**amount** | **int** |  | [optional] 
**currency** | **mobwa_payments_hub_create_transfer_request_CURRENCY_e** |  | [optional] 
**auto_complete** | **int** |  | [optional] 
**destination** | **char \*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


