# SignupAPI

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SignupAPI_signup**](SignupAPI.md#SignupAPI_signup) | **POST** /signup | Signup


# **SignupAPI_signup**
```c
// Signup
//
sign_up_response_t* SignupAPI_signup(apiClient_t *apiClient, sign_up_request_t * sign_up_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**sign_up_request** | **[sign_up_request_t](sign_up_request.md) \*** |  | [optional] 

### Return type

[sign_up_response_t](sign_up_response.md) *


### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

