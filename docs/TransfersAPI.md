# TransfersAPI

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**TransfersAPI_completeTransfer**](TransfersAPI.md#TransfersAPI_completeTransfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer
[**TransfersAPI_createTransfer**](TransfersAPI.md#TransfersAPI_createTransfer) | **POST** /accounts/{accountId}/transfers | Create a transfer
[**TransfersAPI_deleteTransfer**](TransfersAPI.md#TransfersAPI_deleteTransfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer
[**TransfersAPI_getTransfer**](TransfersAPI.md#TransfersAPI_getTransfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer
[**TransfersAPI_listTransfers**](TransfersAPI.md#TransfersAPI_listTransfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account
[**TransfersAPI_updateTransfer**](TransfersAPI.md#TransfersAPI_updateTransfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information


# **TransfersAPI_completeTransfer**
```c
// Complete a transfer
//
transfer_t* TransfersAPI_completeTransfer(apiClient_t *apiClient, char * accountId, char * transferId);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**transferId** | **char \*** | The id of the transfer to operate on | 

### Return type

[transfer_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TransfersAPI_createTransfer**
```c
// Create a transfer
//
transfer_t* TransfersAPI_createTransfer(apiClient_t *apiClient, char * accountId, create_transfer_request_t * create_transfer_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**create_transfer_request** | **[create_transfer_request_t](create_transfer_request.md) \*** |  | [optional] 

### Return type

[transfer_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TransfersAPI_deleteTransfer**
```c
// Delete a transfer
//
transfer_t* TransfersAPI_deleteTransfer(apiClient_t *apiClient, char * accountId, char * transferId);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**transferId** | **char \*** | The id of the transfers to operate on | 

### Return type

[transfer_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TransfersAPI_getTransfer**
```c
// Retrieve information for a specific transfer
//
transfer_t* TransfersAPI_getTransfer(apiClient_t *apiClient, char * accountId, char * transferId);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**transferId** | **char \*** | The id of the transfers to operate on | 

### Return type

[transfer_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TransfersAPI_listTransfers**
```c
// List all transfers related to the account
//
list_t* TransfersAPI_listTransfers(apiClient_t *apiClient, char * accountId);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 

### Return type

[list_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **TransfersAPI_updateTransfer**
```c
// Change transfer information
//
transfer_t* TransfersAPI_updateTransfer(apiClient_t *apiClient, char * accountId, char * transferId, update_transfer_request_t * update_transfer_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**transferId** | **char \*** | The id of the transfers to operate on | 
**update_transfer_request** | **[update_transfer_request_t](update_transfer_request.md) \*** |  | [optional] 

### Return type

[transfer_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

