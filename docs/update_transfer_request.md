# update_transfer_request_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **char \*** |  | [optional] 
**amount** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


