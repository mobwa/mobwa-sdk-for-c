# UsersAPI

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**UsersAPI_createUser**](UsersAPI.md#UsersAPI_createUser) | **POST** /users | Create a user
[**UsersAPI_getUser**](UsersAPI.md#UsersAPI_getUser) | **GET** /users/{userId} | Get user information
[**UsersAPI_listUsers**](UsersAPI.md#UsersAPI_listUsers) | **GET** /users | List all users
[**UsersAPI_updateUser**](UsersAPI.md#UsersAPI_updateUser) | **PUT** /users/{userId} | Update user information


# **UsersAPI_createUser**
```c
// Create a user
//
list_t* UsersAPI_createUser(apiClient_t *apiClient, create_user_request_t * create_user_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**create_user_request** | **[create_user_request_t](create_user_request.md) \*** |  | [optional] 

### Return type

[list_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UsersAPI_getUser**
```c
// Get user information
//
account_t* UsersAPI_getUser(apiClient_t *apiClient, char * userId);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**userId** | **char \*** | The id of the user to operate on | 

### Return type

[account_t](account.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UsersAPI_listUsers**
```c
// List all users
//
list_t* UsersAPI_listUsers(apiClient_t *apiClient);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 

### Return type

[list_t](user.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **UsersAPI_updateUser**
```c
// Update user information
//
account_t* UsersAPI_updateUser(apiClient_t *apiClient, char * userId, update_account_request_t * update_account_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**userId** | **char \*** | The id of the user to operate on | 
**update_account_request** | **[update_account_request_t](update_account_request.md) \*** |  | [optional] 

### Return type

[account_t](account.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

