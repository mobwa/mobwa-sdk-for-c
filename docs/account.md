# account_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **char \*** |  | [optional] 
**name** | **char \*** |  | [optional] 
**created_at** | **char \*** |  | [optional] 
**currency** | **mobwa_payments_hub_account_CURRENCY_e** |  | [optional] 
**updated_at** | **char \*** |  | [optional] 
**balance** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


