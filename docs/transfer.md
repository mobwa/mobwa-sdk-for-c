# transfer_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **char \*** |  | [optional] 
**message** | **char \*** |  | [optional] 
**amount** | **int** |  | [optional] 
**currency** | **mobwa_payments_hub_transfer_CURRENCY_e** |  | [optional] 
**created_at** | **char \*** |  | [optional] 
**updated_at** | **char \*** |  | [optional] 
**status** | **mobwa_payments_hub_transfer_STATUS_e** |  | [optional] 
**auto_complete** | **int** |  | [optional] 
**source** | [**transfer_source_t**](transfer_source.md) \* |  | [optional] 
**destination** | [**transfer_source_t**](transfer_source.md) \* |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


