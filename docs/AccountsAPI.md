# AccountsAPI

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AccountsAPI_createAccount**](AccountsAPI.md#AccountsAPI_createAccount) | **POST** /accounts | Create an account
[**AccountsAPI_getAccount**](AccountsAPI.md#AccountsAPI_getAccount) | **GET** /accounts/{accountId} | Get account information
[**AccountsAPI_listAccount**](AccountsAPI.md#AccountsAPI_listAccount) | **GET** /accounts | List all accounts
[**AccountsAPI_rechargeAccount**](AccountsAPI.md#AccountsAPI_rechargeAccount) | **POST** /accounts/{accountId}/recharge | Recharge the account
[**AccountsAPI_updateAccount**](AccountsAPI.md#AccountsAPI_updateAccount) | **PUT** /accounts/{accountId} | Update account information
[**AccountsAPI_withdrawMoney**](AccountsAPI.md#AccountsAPI_withdrawMoney) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account


# **AccountsAPI_createAccount**
```c
// Create an account
//
list_t* AccountsAPI_createAccount(apiClient_t *apiClient, create_account_request_t * create_account_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**create_account_request** | **[create_account_request_t](create_account_request.md) \*** |  | [optional] 

### Return type

[list_t](transfer.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountsAPI_getAccount**
```c
// Get account information
//
account_t* AccountsAPI_getAccount(apiClient_t *apiClient, char * accountId);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 

### Return type

[account_t](account.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountsAPI_listAccount**
```c
// List all accounts
//
list_t* AccountsAPI_listAccount(apiClient_t *apiClient);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 

### Return type

[list_t](account.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountsAPI_rechargeAccount**
```c
// Recharge the account
//
account_t* AccountsAPI_rechargeAccount(apiClient_t *apiClient, char * accountId, recharge_account_request_t * recharge_account_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**recharge_account_request** | **[recharge_account_request_t](recharge_account_request.md) \*** |  | [optional] 

### Return type

[account_t](account.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountsAPI_updateAccount**
```c
// Update account information
//
account_t* AccountsAPI_updateAccount(apiClient_t *apiClient, char * accountId, update_account_request_t * update_account_request);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**update_account_request** | **[update_account_request_t](update_account_request.md) \*** |  | [optional] 

### Return type

[account_t](account.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **AccountsAPI_withdrawMoney**
```c
// Withdraw money from the account
//
account_t* AccountsAPI_withdrawMoney(apiClient_t *apiClient, char * accountId,  body);
```

### Parameters
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**apiClient** | **apiClient_t \*** | context containing the client configuration | 
**accountId** | **char \*** | The id of the account | 
**body** |  |  | [optional] 

### Return type

[account_t](account.md) *


### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

