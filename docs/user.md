# user_t

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **char \*** |  | [optional] 
**email** | **char \*** |  | [optional] 
**role** | **mobwa_payments_hub_user_ROLE_e** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


