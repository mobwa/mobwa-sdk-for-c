#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/account.h"
#include "../model/create_user_request.h"
#include "../model/error.h"
#include "../model/transfer.h"
#include "../model/update_account_request.h"
#include "../model/user.h"


// Create a user
//
list_t*
UsersAPI_createUser(apiClient_t *apiClient, create_user_request_t * create_user_request );


// Get user information
//
account_t*
UsersAPI_getUser(apiClient_t *apiClient, char * userId );


// List all users
//
list_t*
UsersAPI_listUsers(apiClient_t *apiClient);


// Update user information
//
account_t*
UsersAPI_updateUser(apiClient_t *apiClient, char * userId , update_account_request_t * update_account_request );


