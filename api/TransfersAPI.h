#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/create_transfer_request.h"
#include "../model/error.h"
#include "../model/transfer.h"
#include "../model/update_transfer_request.h"


// Complete a transfer
//
transfer_t*
TransfersAPI_completeTransfer(apiClient_t *apiClient, char * accountId , char * transferId );


// Create a transfer
//
transfer_t*
TransfersAPI_createTransfer(apiClient_t *apiClient, char * accountId , create_transfer_request_t * create_transfer_request );


// Delete a transfer
//
transfer_t*
TransfersAPI_deleteTransfer(apiClient_t *apiClient, char * accountId , char * transferId );


// Retrieve information for a specific transfer
//
transfer_t*
TransfersAPI_getTransfer(apiClient_t *apiClient, char * accountId , char * transferId );


// List all transfers related to the account
//
list_t*
TransfersAPI_listTransfers(apiClient_t *apiClient, char * accountId );


// Change transfer information
//
transfer_t*
TransfersAPI_updateTransfer(apiClient_t *apiClient, char * accountId , char * transferId , update_transfer_request_t * update_transfer_request );


