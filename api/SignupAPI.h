#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/error.h"
#include "../model/sign_up_request.h"
#include "../model/sign_up_response.h"


// Signup
//
sign_up_response_t*
SignupAPI_signup(apiClient_t *apiClient, sign_up_request_t * sign_up_request );


