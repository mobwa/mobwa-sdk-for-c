#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "AccountsAPI.h"

#define MAX_NUMBER_LENGTH 16
#define MAX_BUFFER_LENGTH 4096
#define intToStr(dst, src) \
    do {\
    char dst[256];\
    snprintf(dst, 256, "%ld", (long int)(src));\
}while(0)


// Create an account
//
list_t*
AccountsAPI_createAccount(apiClient_t *apiClient, create_account_request_t * create_account_request )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts");




    // Body Param
    cJSON *localVarSingleItemJSON_create_account_request = NULL;
    if (create_account_request != NULL)
    {
        //string
        localVarSingleItemJSON_create_account_request = create_account_request_convertToJSON(create_account_request);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_create_account_request);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","A paged array of pets");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    cJSON *AccountsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    if(!cJSON_IsArray(AccountsAPIlocalVarJSON)) {
        return 0;//nonprimitive container
    }
    list_t *elementToReturn = list_create();
    cJSON *VarJSON;
    cJSON_ArrayForEach(VarJSON, AccountsAPIlocalVarJSON)
    {
        if(!cJSON_IsObject(VarJSON))
        {
           // return 0;
        }
        char *localVarJSONToChar = cJSON_Print(VarJSON);
        list_addElement(elementToReturn , localVarJSONToChar);
    }

    cJSON_Delete( AccountsAPIlocalVarJSON);
    cJSON_Delete( VarJSON);
    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    if (localVarSingleItemJSON_create_account_request) {
        cJSON_Delete(localVarSingleItemJSON_create_account_request);
        localVarSingleItemJSON_create_account_request = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Get account information
//
account_t*
AccountsAPI_getAccount(apiClient_t *apiClient, char * accountId )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);


    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "GET");

    if (apiClient->response_code == 200) {
        printf("%s\n","Expected response to a valid request");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *AccountsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    account_t *elementToReturn = account_parseFromJSON(AccountsAPIlocalVarJSON);
    cJSON_Delete(AccountsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    free(localVarToReplace_accountId);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// List all accounts
//
list_t*
AccountsAPI_listAccount(apiClient_t *apiClient)
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts");



    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "GET");

    if (apiClient->response_code == 200) {
        printf("%s\n","A paged array of pets");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    cJSON *AccountsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    if(!cJSON_IsArray(AccountsAPIlocalVarJSON)) {
        return 0;//nonprimitive container
    }
    list_t *elementToReturn = list_create();
    cJSON *VarJSON;
    cJSON_ArrayForEach(VarJSON, AccountsAPIlocalVarJSON)
    {
        if(!cJSON_IsObject(VarJSON))
        {
           // return 0;
        }
        char *localVarJSONToChar = cJSON_Print(VarJSON);
        list_addElement(elementToReturn , localVarJSONToChar);
    }

    cJSON_Delete( AccountsAPIlocalVarJSON);
    cJSON_Delete( VarJSON);
    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Recharge the account
//
account_t*
AccountsAPI_rechargeAccount(apiClient_t *apiClient, char * accountId , recharge_account_request_t * recharge_account_request )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/recharge")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/recharge");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);



    // Body Param
    cJSON *localVarSingleItemJSON_recharge_account_request = NULL;
    if (recharge_account_request != NULL)
    {
        //string
        localVarSingleItemJSON_recharge_account_request = recharge_account_request_convertToJSON(recharge_account_request);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_recharge_account_request);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","Expected response to a valid request");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *AccountsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    account_t *elementToReturn = account_parseFromJSON(AccountsAPIlocalVarJSON);
    cJSON_Delete(AccountsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    free(localVarToReplace_accountId);
    if (localVarSingleItemJSON_recharge_account_request) {
        cJSON_Delete(localVarSingleItemJSON_recharge_account_request);
        localVarSingleItemJSON_recharge_account_request = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Update account information
//
account_t*
AccountsAPI_updateAccount(apiClient_t *apiClient, char * accountId , update_account_request_t * update_account_request )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);



    // Body Param
    cJSON *localVarSingleItemJSON_update_account_request = NULL;
    if (update_account_request != NULL)
    {
        //string
        localVarSingleItemJSON_update_account_request = update_account_request_convertToJSON(update_account_request);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_update_account_request);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "PUT");

    if (apiClient->response_code == 200) {
        printf("%s\n","Expected response to a valid request");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *AccountsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    account_t *elementToReturn = account_parseFromJSON(AccountsAPIlocalVarJSON);
    cJSON_Delete(AccountsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    free(localVarToReplace_accountId);
    if (localVarSingleItemJSON_update_account_request) {
        cJSON_Delete(localVarSingleItemJSON_update_account_request);
        localVarSingleItemJSON_update_account_request = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Withdraw money from the account
//
account_t*
AccountsAPI_withdrawMoney(apiClient_t *apiClient, char * accountId ,  body )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/withdraw")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/withdraw");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);



    // Body Param
    cJSON *localVarSingleItemJSON_body = NULL;
    if (body != NULL)
    {
        //string
        localVarSingleItemJSON_body = recharge_account_request_convertToJSON(body);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_body);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","Expected response to a valid request");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *AccountsAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    account_t *elementToReturn = account_parseFromJSON(AccountsAPIlocalVarJSON);
    cJSON_Delete(AccountsAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    free(localVarToReplace_accountId);
    if (localVarSingleItemJSON_body) {
        cJSON_Delete(localVarSingleItemJSON_body);
        localVarSingleItemJSON_body = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

