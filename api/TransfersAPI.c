#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "TransfersAPI.h"

#define MAX_NUMBER_LENGTH 16
#define MAX_BUFFER_LENGTH 4096
#define intToStr(dst, src) \
    do {\
    char dst[256];\
    snprintf(dst, 256, "%ld", (long int)(src));\
}while(0)


// Complete a transfer
//
transfer_t*
TransfersAPI_completeTransfer(apiClient_t *apiClient, char * accountId , char * transferId )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/transfers/{transferId}/complete")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/transfers/{transferId}/complete");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);

    // Path Params
    long sizeOfPathParams_transferId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ transferId }");
    if(transferId == NULL) {
        goto end;
    }
    char* localVarToReplace_transferId = malloc(sizeOfPathParams_transferId);
    sprintf(localVarToReplace_transferId, "{%s}", "transferId");

    localVarPath = strReplace(localVarPath, localVarToReplace_transferId, transferId);


    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","Expected response to a valid request");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *TransfersAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    transfer_t *elementToReturn = transfer_parseFromJSON(TransfersAPIlocalVarJSON);
    cJSON_Delete(TransfersAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    free(localVarToReplace_accountId);
    free(localVarToReplace_transferId);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Create a transfer
//
transfer_t*
TransfersAPI_createTransfer(apiClient_t *apiClient, char * accountId , create_transfer_request_t * create_transfer_request )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/transfers")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/transfers");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);



    // Body Param
    cJSON *localVarSingleItemJSON_create_transfer_request = NULL;
    if (create_transfer_request != NULL)
    {
        //string
        localVarSingleItemJSON_create_transfer_request = create_transfer_request_convertToJSON(create_transfer_request);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_create_transfer_request);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 201) {
        printf("%s\n","Transfer successfully created");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *TransfersAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    transfer_t *elementToReturn = transfer_parseFromJSON(TransfersAPIlocalVarJSON);
    cJSON_Delete(TransfersAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    free(localVarToReplace_accountId);
    if (localVarSingleItemJSON_create_transfer_request) {
        cJSON_Delete(localVarSingleItemJSON_create_transfer_request);
        localVarSingleItemJSON_create_transfer_request = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Delete a transfer
//
transfer_t*
TransfersAPI_deleteTransfer(apiClient_t *apiClient, char * accountId , char * transferId )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/transfers/{transferId}")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/transfers/{transferId}");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);

    // Path Params
    long sizeOfPathParams_transferId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ transferId }");
    if(transferId == NULL) {
        goto end;
    }
    char* localVarToReplace_transferId = malloc(sizeOfPathParams_transferId);
    sprintf(localVarToReplace_transferId, "{%s}", "transferId");

    localVarPath = strReplace(localVarPath, localVarToReplace_transferId, transferId);


    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "DELETE");

    if (apiClient->response_code == 200) {
        printf("%s\n","Null response");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *TransfersAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    transfer_t *elementToReturn = transfer_parseFromJSON(TransfersAPIlocalVarJSON);
    cJSON_Delete(TransfersAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    free(localVarToReplace_accountId);
    free(localVarToReplace_transferId);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Retrieve information for a specific transfer
//
transfer_t*
TransfersAPI_getTransfer(apiClient_t *apiClient, char * accountId , char * transferId )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/transfers/{transferId}")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/transfers/{transferId}");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);

    // Path Params
    long sizeOfPathParams_transferId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ transferId }");
    if(transferId == NULL) {
        goto end;
    }
    char* localVarToReplace_transferId = malloc(sizeOfPathParams_transferId);
    sprintf(localVarToReplace_transferId, "{%s}", "transferId");

    localVarPath = strReplace(localVarPath, localVarToReplace_transferId, transferId);


    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "GET");

    if (apiClient->response_code == 200) {
        printf("%s\n","Expected response to a valid request");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *TransfersAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    transfer_t *elementToReturn = transfer_parseFromJSON(TransfersAPIlocalVarJSON);
    cJSON_Delete(TransfersAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    free(localVarToReplace_accountId);
    free(localVarToReplace_transferId);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// List all transfers related to the account
//
list_t*
TransfersAPI_listTransfers(apiClient_t *apiClient, char * accountId )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = NULL;
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/transfers")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/transfers");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);


    list_addElement(localVarHeaderType,"application/json"); //produces
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "GET");

    if (apiClient->response_code == 200) {
        printf("%s\n","A list of transfers related to the the account");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    cJSON *TransfersAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    if(!cJSON_IsArray(TransfersAPIlocalVarJSON)) {
        return 0;//nonprimitive container
    }
    list_t *elementToReturn = list_create();
    cJSON *VarJSON;
    cJSON_ArrayForEach(VarJSON, TransfersAPIlocalVarJSON)
    {
        if(!cJSON_IsObject(VarJSON))
        {
           // return 0;
        }
        char *localVarJSONToChar = cJSON_Print(VarJSON);
        list_addElement(elementToReturn , localVarJSONToChar);
    }

    cJSON_Delete( TransfersAPIlocalVarJSON);
    cJSON_Delete( VarJSON);
    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    
    free(localVarPath);
    free(localVarToReplace_accountId);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

// Change transfer information
//
transfer_t*
TransfersAPI_updateTransfer(apiClient_t *apiClient, char * accountId , char * transferId , update_transfer_request_t * update_transfer_request )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/accounts/{accountId}/transfers/{transferId}")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/accounts/{accountId}/transfers/{transferId}");


    // Path Params
    long sizeOfPathParams_accountId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ accountId }");
    if(accountId == NULL) {
        goto end;
    }
    char* localVarToReplace_accountId = malloc(sizeOfPathParams_accountId);
    sprintf(localVarToReplace_accountId, "{%s}", "accountId");

    localVarPath = strReplace(localVarPath, localVarToReplace_accountId, accountId);

    // Path Params
    long sizeOfPathParams_transferId = strlen(accountId)+3 + strlen(transferId)+3 + strlen("{ transferId }");
    if(transferId == NULL) {
        goto end;
    }
    char* localVarToReplace_transferId = malloc(sizeOfPathParams_transferId);
    sprintf(localVarToReplace_transferId, "{%s}", "transferId");

    localVarPath = strReplace(localVarPath, localVarToReplace_transferId, transferId);



    // Body Param
    cJSON *localVarSingleItemJSON_update_transfer_request = NULL;
    if (update_transfer_request != NULL)
    {
        //string
        localVarSingleItemJSON_update_transfer_request = update_transfer_request_convertToJSON(update_transfer_request);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_update_transfer_request);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "PUT");

    if (apiClient->response_code == 201) {
        printf("%s\n","Null response");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *TransfersAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    transfer_t *elementToReturn = transfer_parseFromJSON(TransfersAPIlocalVarJSON);
    cJSON_Delete(TransfersAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    free(localVarToReplace_accountId);
    free(localVarToReplace_transferId);
    if (localVarSingleItemJSON_update_transfer_request) {
        cJSON_Delete(localVarSingleItemJSON_update_transfer_request);
        localVarSingleItemJSON_update_transfer_request = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

