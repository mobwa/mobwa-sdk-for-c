#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "SignupAPI.h"

#define MAX_NUMBER_LENGTH 16
#define MAX_BUFFER_LENGTH 4096
#define intToStr(dst, src) \
    do {\
    char dst[256];\
    snprintf(dst, 256, "%ld", (long int)(src));\
}while(0)


// Signup
//
sign_up_response_t*
SignupAPI_signup(apiClient_t *apiClient, sign_up_request_t * sign_up_request )
{
    list_t    *localVarQueryParameters = NULL;
    list_t    *localVarHeaderParameters = NULL;
    list_t    *localVarFormParameters = NULL;
    list_t *localVarHeaderType = list_create();
    list_t *localVarContentType = list_create();
    char      *localVarBodyParameters = NULL;

    // create the path
    long sizeOfPath = strlen("/signup")+1;
    char *localVarPath = malloc(sizeOfPath);
    snprintf(localVarPath, sizeOfPath, "/signup");




    // Body Param
    cJSON *localVarSingleItemJSON_sign_up_request = NULL;
    if (sign_up_request != NULL)
    {
        //string
        localVarSingleItemJSON_sign_up_request = sign_up_request_convertToJSON(sign_up_request);
        localVarBodyParameters = cJSON_Print(localVarSingleItemJSON_sign_up_request);
    }
    list_addElement(localVarHeaderType,"application/json"); //produces
    list_addElement(localVarContentType,"application/json"); //consumes
    apiClient_invoke(apiClient,
                    localVarPath,
                    localVarQueryParameters,
                    localVarHeaderParameters,
                    localVarFormParameters,
                    localVarHeaderType,
                    localVarContentType,
                    localVarBodyParameters,
                    "POST");

    if (apiClient->response_code == 200) {
        printf("%s\n","Information related to the enrolled user");
    }
    if (apiClient->response_code == 0) {
        printf("%s\n","unexpected error");
    }
    //nonprimitive not container
    cJSON *SignupAPIlocalVarJSON = cJSON_Parse(apiClient->dataReceived);
    sign_up_response_t *elementToReturn = sign_up_response_parseFromJSON(SignupAPIlocalVarJSON);
    cJSON_Delete(SignupAPIlocalVarJSON);
    if(elementToReturn == NULL) {
        // return 0;
    }

    //return type
    if (apiClient->dataReceived) {
        free(apiClient->dataReceived);
        apiClient->dataReceived = NULL;
        apiClient->dataReceivedLen = 0;
    }
    
    
    
    list_free(localVarHeaderType);
    list_free(localVarContentType);
    free(localVarPath);
    if (localVarSingleItemJSON_sign_up_request) {
        cJSON_Delete(localVarSingleItemJSON_sign_up_request);
        localVarSingleItemJSON_sign_up_request = NULL;
    }
    free(localVarBodyParameters);
    return elementToReturn;
end:
    free(localVarPath);
    return NULL;

}

