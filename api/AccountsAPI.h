#include <stdlib.h>
#include <stdio.h>
#include "../include/apiClient.h"
#include "../include/list.h"
#include "../external/cJSON.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"
#include "../model/account.h"
#include "../model/create_account_request.h"
#include "../model/error.h"
#include "../model/recharge_account_request.h"
#include "../model/transfer.h"
#include "../model/update_account_request.h"


// Create an account
//
list_t*
AccountsAPI_createAccount(apiClient_t *apiClient, create_account_request_t * create_account_request );


// Get account information
//
account_t*
AccountsAPI_getAccount(apiClient_t *apiClient, char * accountId );


// List all accounts
//
list_t*
AccountsAPI_listAccount(apiClient_t *apiClient);


// Recharge the account
//
account_t*
AccountsAPI_rechargeAccount(apiClient_t *apiClient, char * accountId , recharge_account_request_t * recharge_account_request );


// Update account information
//
account_t*
AccountsAPI_updateAccount(apiClient_t *apiClient, char * accountId , update_account_request_t * update_account_request );


// Withdraw money from the account
//
account_t*
AccountsAPI_withdrawMoney(apiClient_t *apiClient, char * accountId ,  body );


