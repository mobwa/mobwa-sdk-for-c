#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sign_up_response.h"


char* rolesign_up_response_ToString(mobwa_payments_hub_sign_up_response_ROLE_e role) {
    char* roleArray[] =  { "NULL", "customer", "admin" };
	return roleArray[role];
}

mobwa_payments_hub_sign_up_response_ROLE_e rolesign_up_response_FromString(char* role){
    int stringToReturn = 0;
    char *roleArray[] =  { "NULL", "customer", "admin" };
    size_t sizeofArray = sizeof(roleArray) / sizeof(roleArray[0]);
    while(stringToReturn < sizeofArray) {
        if(strcmp(role, roleArray[stringToReturn]) == 0) {
            return stringToReturn;
        }
        stringToReturn++;
    }
    return 0;
}

sign_up_response_t *sign_up_response_create(
    char *username,
    char *email,
    mobwa_payments_hub_sign_up_response_ROLE_e role,
    list_t *accounts
    ) {
    sign_up_response_t *sign_up_response_local_var = malloc(sizeof(sign_up_response_t));
    if (!sign_up_response_local_var) {
        return NULL;
    }
    sign_up_response_local_var->username = username;
    sign_up_response_local_var->email = email;
    sign_up_response_local_var->role = role;
    sign_up_response_local_var->accounts = accounts;

    return sign_up_response_local_var;
}


void sign_up_response_free(sign_up_response_t *sign_up_response) {
    if(NULL == sign_up_response){
        return ;
    }
    listEntry_t *listEntry;
    if (sign_up_response->username) {
        free(sign_up_response->username);
        sign_up_response->username = NULL;
    }
    if (sign_up_response->email) {
        free(sign_up_response->email);
        sign_up_response->email = NULL;
    }
    if (sign_up_response->accounts) {
        list_ForEach(listEntry, sign_up_response->accounts) {
            account_free(listEntry->data);
        }
        list_free(sign_up_response->accounts);
        sign_up_response->accounts = NULL;
    }
    free(sign_up_response);
}

cJSON *sign_up_response_convertToJSON(sign_up_response_t *sign_up_response) {
    cJSON *item = cJSON_CreateObject();

    // sign_up_response->username
    if(sign_up_response->username) { 
    if(cJSON_AddStringToObject(item, "username", sign_up_response->username) == NULL) {
    goto fail; //String
    }
     } 


    // sign_up_response->email
    if(sign_up_response->email) { 
    if(cJSON_AddStringToObject(item, "email", sign_up_response->email) == NULL) {
    goto fail; //String
    }
     } 


    // sign_up_response->role
    
    if(cJSON_AddStringToObject(item, "role", rolesign_up_response_ToString(sign_up_response->role)) == NULL)
    {
    goto fail; //Enum
    }
    


    // sign_up_response->accounts
    if(sign_up_response->accounts) { 
    cJSON *accounts = cJSON_AddArrayToObject(item, "accounts");
    if(accounts == NULL) {
    goto fail; //nonprimitive container
    }

    listEntry_t *accountsListEntry;
    if (sign_up_response->accounts) {
    list_ForEach(accountsListEntry, sign_up_response->accounts) {
    cJSON *itemLocal = account_convertToJSON(accountsListEntry->data);
    if(itemLocal == NULL) {
    goto fail;
    }
    cJSON_AddItemToArray(accounts, itemLocal);
    }
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

sign_up_response_t *sign_up_response_parseFromJSON(cJSON *sign_up_responseJSON){

    sign_up_response_t *sign_up_response_local_var = NULL;

    // sign_up_response->username
    cJSON *username = cJSON_GetObjectItemCaseSensitive(sign_up_responseJSON, "username");
    if (username) { 
    if(!cJSON_IsString(username))
    {
    goto end; //String
    }
    }

    // sign_up_response->email
    cJSON *email = cJSON_GetObjectItemCaseSensitive(sign_up_responseJSON, "email");
    if (email) { 
    if(!cJSON_IsString(email))
    {
    goto end; //String
    }
    }

    // sign_up_response->role
    cJSON *role = cJSON_GetObjectItemCaseSensitive(sign_up_responseJSON, "role");
    mobwa_payments_hub_sign_up_response_ROLE_e roleVariable;
    if (role) { 
    if(!cJSON_IsString(role))
    {
    goto end; //Enum
    }
    roleVariable = rolesign_up_response_FromString(role->valuestring);
    }

    // sign_up_response->accounts
    cJSON *accounts = cJSON_GetObjectItemCaseSensitive(sign_up_responseJSON, "accounts");
    list_t *accountsList;
    if (accounts) { 
    cJSON *accounts_local_nonprimitive;
    if(!cJSON_IsArray(accounts)){
        goto end; //nonprimitive container
    }

    accountsList = list_create();

    cJSON_ArrayForEach(accounts_local_nonprimitive,accounts )
    {
        if(!cJSON_IsObject(accounts_local_nonprimitive)){
            goto end;
        }
        account_t *accountsItem = account_parseFromJSON(accounts_local_nonprimitive);

        list_addElement(accountsList, accountsItem);
    }
    }


    sign_up_response_local_var = sign_up_response_create (
        username ? strdup(username->valuestring) : NULL,
        email ? strdup(email->valuestring) : NULL,
        role ? roleVariable : -1,
        accounts ? accountsList : NULL
        );

    return sign_up_response_local_var;
end:
    return NULL;

}
