/*
 * sign_up_response.h
 *
 * 
 */

#ifndef _sign_up_response_H_
#define _sign_up_response_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct sign_up_response_t sign_up_response_t;

#include "account.h"

// Enum ROLE for sign_up_response

typedef enum  { mobwa_payments_hub_sign_up_response_ROLE_NULL = 0, mobwa_payments_hub_sign_up_response_ROLE_customer, mobwa_payments_hub_sign_up_response_ROLE_admin } mobwa_payments_hub_sign_up_response_ROLE_e;

char* sign_up_response_role_ToString(mobwa_payments_hub_sign_up_response_ROLE_e role);

mobwa_payments_hub_sign_up_response_ROLE_e sign_up_response_role_FromString(char* role);



typedef struct sign_up_response_t {
    char *username; // string
    char *email; // string
    mobwa_payments_hub_sign_up_response_ROLE_e role; //enum
    list_t *accounts; //nonprimitive container

} sign_up_response_t;

sign_up_response_t *sign_up_response_create(
    char *username,
    char *email,
    mobwa_payments_hub_sign_up_response_ROLE_e role,
    list_t *accounts
);

void sign_up_response_free(sign_up_response_t *sign_up_response);

sign_up_response_t *sign_up_response_parseFromJSON(cJSON *sign_up_responseJSON);

cJSON *sign_up_response_convertToJSON(sign_up_response_t *sign_up_response);

#endif /* _sign_up_response_H_ */

