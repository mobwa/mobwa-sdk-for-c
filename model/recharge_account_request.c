#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "recharge_account_request.h"



recharge_account_request_t *recharge_account_request_create(
    int amout
    ) {
    recharge_account_request_t *recharge_account_request_local_var = malloc(sizeof(recharge_account_request_t));
    if (!recharge_account_request_local_var) {
        return NULL;
    }
    recharge_account_request_local_var->amout = amout;

    return recharge_account_request_local_var;
}


void recharge_account_request_free(recharge_account_request_t *recharge_account_request) {
    if(NULL == recharge_account_request){
        return ;
    }
    listEntry_t *listEntry;
    free(recharge_account_request);
}

cJSON *recharge_account_request_convertToJSON(recharge_account_request_t *recharge_account_request) {
    cJSON *item = cJSON_CreateObject();

    // recharge_account_request->amout
    if(recharge_account_request->amout) { 
    if(cJSON_AddNumberToObject(item, "amout", recharge_account_request->amout) == NULL) {
    goto fail; //Numeric
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

recharge_account_request_t *recharge_account_request_parseFromJSON(cJSON *recharge_account_requestJSON){

    recharge_account_request_t *recharge_account_request_local_var = NULL;

    // recharge_account_request->amout
    cJSON *amout = cJSON_GetObjectItemCaseSensitive(recharge_account_requestJSON, "amout");
    if (amout) { 
    if(!cJSON_IsNumber(amout))
    {
    goto end; //Numeric
    }
    }


    recharge_account_request_local_var = recharge_account_request_create (
        amout ? amout->valuedouble : 0
        );

    return recharge_account_request_local_var;
end:
    return NULL;

}
