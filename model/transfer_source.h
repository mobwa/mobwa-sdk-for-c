/*
 * transfer_source.h
 *
 * 
 */

#ifndef _transfer_source_H_
#define _transfer_source_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct transfer_source_t transfer_source_t;




typedef struct transfer_source_t {
    char *account_id; // string

} transfer_source_t;

transfer_source_t *transfer_source_create(
    char *account_id
);

void transfer_source_free(transfer_source_t *transfer_source);

transfer_source_t *transfer_source_parseFromJSON(cJSON *transfer_sourceJSON);

cJSON *transfer_source_convertToJSON(transfer_source_t *transfer_source);

#endif /* _transfer_source_H_ */

