#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "account.h"


char* currencyaccount_ToString(mobwa_payments_hub_account_CURRENCY_e currency) {
    char* currencyArray[] =  { "NULL", "USD", "SGD" };
	return currencyArray[currency];
}

mobwa_payments_hub_account_CURRENCY_e currencyaccount_FromString(char* currency){
    int stringToReturn = 0;
    char *currencyArray[] =  { "NULL", "USD", "SGD" };
    size_t sizeofArray = sizeof(currencyArray) / sizeof(currencyArray[0]);
    while(stringToReturn < sizeofArray) {
        if(strcmp(currency, currencyArray[stringToReturn]) == 0) {
            return stringToReturn;
        }
        stringToReturn++;
    }
    return 0;
}

account_t *account_create(
    char *id,
    char *name,
    char *created_at,
    mobwa_payments_hub_account_CURRENCY_e currency,
    char *updated_at,
    int balance
    ) {
    account_t *account_local_var = malloc(sizeof(account_t));
    if (!account_local_var) {
        return NULL;
    }
    account_local_var->id = id;
    account_local_var->name = name;
    account_local_var->created_at = created_at;
    account_local_var->currency = currency;
    account_local_var->updated_at = updated_at;
    account_local_var->balance = balance;

    return account_local_var;
}


void account_free(account_t *account) {
    if(NULL == account){
        return ;
    }
    listEntry_t *listEntry;
    if (account->id) {
        free(account->id);
        account->id = NULL;
    }
    if (account->name) {
        free(account->name);
        account->name = NULL;
    }
    if (account->created_at) {
        free(account->created_at);
        account->created_at = NULL;
    }
    if (account->updated_at) {
        free(account->updated_at);
        account->updated_at = NULL;
    }
    free(account);
}

cJSON *account_convertToJSON(account_t *account) {
    cJSON *item = cJSON_CreateObject();

    // account->id
    if(account->id) { 
    if(cJSON_AddStringToObject(item, "id", account->id) == NULL) {
    goto fail; //String
    }
     } 


    // account->name
    if(account->name) { 
    if(cJSON_AddStringToObject(item, "name", account->name) == NULL) {
    goto fail; //String
    }
     } 


    // account->created_at
    if(account->created_at) { 
    if(cJSON_AddStringToObject(item, "created_at", account->created_at) == NULL) {
    goto fail; //String
    }
     } 


    // account->currency
    
    if(cJSON_AddStringToObject(item, "currency", currencyaccount_ToString(account->currency)) == NULL)
    {
    goto fail; //Enum
    }
    


    // account->updated_at
    if(account->updated_at) { 
    if(cJSON_AddStringToObject(item, "updated_at", account->updated_at) == NULL) {
    goto fail; //String
    }
     } 


    // account->balance
    if(account->balance) { 
    if(cJSON_AddNumberToObject(item, "balance", account->balance) == NULL) {
    goto fail; //Numeric
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

account_t *account_parseFromJSON(cJSON *accountJSON){

    account_t *account_local_var = NULL;

    // account->id
    cJSON *id = cJSON_GetObjectItemCaseSensitive(accountJSON, "id");
    if (id) { 
    if(!cJSON_IsString(id))
    {
    goto end; //String
    }
    }

    // account->name
    cJSON *name = cJSON_GetObjectItemCaseSensitive(accountJSON, "name");
    if (name) { 
    if(!cJSON_IsString(name))
    {
    goto end; //String
    }
    }

    // account->created_at
    cJSON *created_at = cJSON_GetObjectItemCaseSensitive(accountJSON, "created_at");
    if (created_at) { 
    if(!cJSON_IsString(created_at))
    {
    goto end; //String
    }
    }

    // account->currency
    cJSON *currency = cJSON_GetObjectItemCaseSensitive(accountJSON, "currency");
    mobwa_payments_hub_account_CURRENCY_e currencyVariable;
    if (currency) { 
    if(!cJSON_IsString(currency))
    {
    goto end; //Enum
    }
    currencyVariable = currencyaccount_FromString(currency->valuestring);
    }

    // account->updated_at
    cJSON *updated_at = cJSON_GetObjectItemCaseSensitive(accountJSON, "updated_at");
    if (updated_at) { 
    if(!cJSON_IsString(updated_at))
    {
    goto end; //String
    }
    }

    // account->balance
    cJSON *balance = cJSON_GetObjectItemCaseSensitive(accountJSON, "balance");
    if (balance) { 
    if(!cJSON_IsNumber(balance))
    {
    goto end; //Numeric
    }
    }


    account_local_var = account_create (
        id ? strdup(id->valuestring) : NULL,
        name ? strdup(name->valuestring) : NULL,
        created_at ? strdup(created_at->valuestring) : NULL,
        currency ? currencyVariable : -1,
        updated_at ? strdup(updated_at->valuestring) : NULL,
        balance ? balance->valuedouble : 0
        );

    return account_local_var;
end:
    return NULL;

}
