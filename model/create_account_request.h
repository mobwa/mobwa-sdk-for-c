/*
 * create_account_request.h
 *
 * 
 */

#ifndef _create_account_request_H_
#define _create_account_request_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct create_account_request_t create_account_request_t;




typedef struct create_account_request_t {
    char *name; // string

} create_account_request_t;

create_account_request_t *create_account_request_create(
    char *name
);

void create_account_request_free(create_account_request_t *create_account_request);

create_account_request_t *create_account_request_parseFromJSON(cJSON *create_account_requestJSON);

cJSON *create_account_request_convertToJSON(create_account_request_t *create_account_request);

#endif /* _create_account_request_H_ */

