/*
 * transfer.h
 *
 * 
 */

#ifndef _transfer_H_
#define _transfer_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct transfer_t transfer_t;

#include "transfer_source.h"

// Enum CURRENCY for transfer

typedef enum  { mobwa_payments_hub_transfer_CURRENCY_NULL = 0, mobwa_payments_hub_transfer_CURRENCY_SGD, mobwa_payments_hub_transfer_CURRENCY_USD } mobwa_payments_hub_transfer_CURRENCY_e;

char* transfer_currency_ToString(mobwa_payments_hub_transfer_CURRENCY_e currency);

mobwa_payments_hub_transfer_CURRENCY_e transfer_currency_FromString(char* currency);

// Enum STATUS for transfer

typedef enum  { mobwa_payments_hub_transfer_STATUS_NULL = 0, mobwa_payments_hub_transfer_STATUS_PENDING, mobwa_payments_hub_transfer_STATUS_SUCCEEDED, mobwa_payments_hub_transfer_STATUS_FAILED } mobwa_payments_hub_transfer_STATUS_e;

char* transfer_status_ToString(mobwa_payments_hub_transfer_STATUS_e status);

mobwa_payments_hub_transfer_STATUS_e transfer_status_FromString(char* status);



typedef struct transfer_t {
    char *id; // string
    char *message; // string
    int amount; //numeric
    mobwa_payments_hub_transfer_CURRENCY_e currency; //enum
    char *created_at; // string
    char *updated_at; // string
    mobwa_payments_hub_transfer_STATUS_e status; //enum
    int auto_complete; //boolean
    struct transfer_source_t *source; //model
    struct transfer_source_t *destination; //model

} transfer_t;

transfer_t *transfer_create(
    char *id,
    char *message,
    int amount,
    mobwa_payments_hub_transfer_CURRENCY_e currency,
    char *created_at,
    char *updated_at,
    mobwa_payments_hub_transfer_STATUS_e status,
    int auto_complete,
    transfer_source_t *source,
    transfer_source_t *destination
);

void transfer_free(transfer_t *transfer);

transfer_t *transfer_parseFromJSON(cJSON *transferJSON);

cJSON *transfer_convertToJSON(transfer_t *transfer);

#endif /* _transfer_H_ */

