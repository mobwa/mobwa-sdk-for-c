/*
 * update_transfer_request.h
 *
 * 
 */

#ifndef _update_transfer_request_H_
#define _update_transfer_request_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct update_transfer_request_t update_transfer_request_t;




typedef struct update_transfer_request_t {
    char *message; // string
    int amount; //numeric

} update_transfer_request_t;

update_transfer_request_t *update_transfer_request_create(
    char *message,
    int amount
);

void update_transfer_request_free(update_transfer_request_t *update_transfer_request);

update_transfer_request_t *update_transfer_request_parseFromJSON(cJSON *update_transfer_requestJSON);

cJSON *update_transfer_request_convertToJSON(update_transfer_request_t *update_transfer_request);

#endif /* _update_transfer_request_H_ */

