/*
 * update_account_request.h
 *
 * 
 */

#ifndef _update_account_request_H_
#define _update_account_request_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct update_account_request_t update_account_request_t;




typedef struct update_account_request_t {
    char *name; // string

} update_account_request_t;

update_account_request_t *update_account_request_create(
    char *name
);

void update_account_request_free(update_account_request_t *update_account_request);

update_account_request_t *update_account_request_parseFromJSON(cJSON *update_account_requestJSON);

cJSON *update_account_request_convertToJSON(update_account_request_t *update_account_request);

#endif /* _update_account_request_H_ */

