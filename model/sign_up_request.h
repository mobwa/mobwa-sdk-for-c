/*
 * sign_up_request.h
 *
 * 
 */

#ifndef _sign_up_request_H_
#define _sign_up_request_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct sign_up_request_t sign_up_request_t;




typedef struct sign_up_request_t {
    char *username; // string
    char *email; // string
    char *password; // string

} sign_up_request_t;

sign_up_request_t *sign_up_request_create(
    char *username,
    char *email,
    char *password
);

void sign_up_request_free(sign_up_request_t *sign_up_request);

sign_up_request_t *sign_up_request_parseFromJSON(cJSON *sign_up_requestJSON);

cJSON *sign_up_request_convertToJSON(sign_up_request_t *sign_up_request);

#endif /* _sign_up_request_H_ */

