#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "create_account_request.h"



create_account_request_t *create_account_request_create(
    char *name
    ) {
    create_account_request_t *create_account_request_local_var = malloc(sizeof(create_account_request_t));
    if (!create_account_request_local_var) {
        return NULL;
    }
    create_account_request_local_var->name = name;

    return create_account_request_local_var;
}


void create_account_request_free(create_account_request_t *create_account_request) {
    if(NULL == create_account_request){
        return ;
    }
    listEntry_t *listEntry;
    if (create_account_request->name) {
        free(create_account_request->name);
        create_account_request->name = NULL;
    }
    free(create_account_request);
}

cJSON *create_account_request_convertToJSON(create_account_request_t *create_account_request) {
    cJSON *item = cJSON_CreateObject();

    // create_account_request->name
    if(create_account_request->name) { 
    if(cJSON_AddStringToObject(item, "name", create_account_request->name) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

create_account_request_t *create_account_request_parseFromJSON(cJSON *create_account_requestJSON){

    create_account_request_t *create_account_request_local_var = NULL;

    // create_account_request->name
    cJSON *name = cJSON_GetObjectItemCaseSensitive(create_account_requestJSON, "name");
    if (name) { 
    if(!cJSON_IsString(name))
    {
    goto end; //String
    }
    }


    create_account_request_local_var = create_account_request_create (
        name ? strdup(name->valuestring) : NULL
        );

    return create_account_request_local_var;
end:
    return NULL;

}
