#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "update_transfer_request.h"



update_transfer_request_t *update_transfer_request_create(
    char *message,
    int amount
    ) {
    update_transfer_request_t *update_transfer_request_local_var = malloc(sizeof(update_transfer_request_t));
    if (!update_transfer_request_local_var) {
        return NULL;
    }
    update_transfer_request_local_var->message = message;
    update_transfer_request_local_var->amount = amount;

    return update_transfer_request_local_var;
}


void update_transfer_request_free(update_transfer_request_t *update_transfer_request) {
    if(NULL == update_transfer_request){
        return ;
    }
    listEntry_t *listEntry;
    if (update_transfer_request->message) {
        free(update_transfer_request->message);
        update_transfer_request->message = NULL;
    }
    free(update_transfer_request);
}

cJSON *update_transfer_request_convertToJSON(update_transfer_request_t *update_transfer_request) {
    cJSON *item = cJSON_CreateObject();

    // update_transfer_request->message
    if(update_transfer_request->message) { 
    if(cJSON_AddStringToObject(item, "message", update_transfer_request->message) == NULL) {
    goto fail; //String
    }
     } 


    // update_transfer_request->amount
    if(update_transfer_request->amount) { 
    if(cJSON_AddNumberToObject(item, "amount", update_transfer_request->amount) == NULL) {
    goto fail; //Numeric
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

update_transfer_request_t *update_transfer_request_parseFromJSON(cJSON *update_transfer_requestJSON){

    update_transfer_request_t *update_transfer_request_local_var = NULL;

    // update_transfer_request->message
    cJSON *message = cJSON_GetObjectItemCaseSensitive(update_transfer_requestJSON, "message");
    if (message) { 
    if(!cJSON_IsString(message))
    {
    goto end; //String
    }
    }

    // update_transfer_request->amount
    cJSON *amount = cJSON_GetObjectItemCaseSensitive(update_transfer_requestJSON, "amount");
    if (amount) { 
    if(!cJSON_IsNumber(amount))
    {
    goto end; //Numeric
    }
    }


    update_transfer_request_local_var = update_transfer_request_create (
        message ? strdup(message->valuestring) : NULL,
        amount ? amount->valuedouble : 0
        );

    return update_transfer_request_local_var;
end:
    return NULL;

}
