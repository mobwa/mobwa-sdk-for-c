/*
 * user.h
 *
 * 
 */

#ifndef _user_H_
#define _user_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct user_t user_t;


// Enum ROLE for user

typedef enum  { mobwa_payments_hub_user_ROLE_NULL = 0, mobwa_payments_hub_user_ROLE_CUSTOMER, mobwa_payments_hub_user_ROLE_ADMIN } mobwa_payments_hub_user_ROLE_e;

char* user_role_ToString(mobwa_payments_hub_user_ROLE_e role);

mobwa_payments_hub_user_ROLE_e user_role_FromString(char* role);



typedef struct user_t {
    char *username; // string
    char *email; // string
    mobwa_payments_hub_user_ROLE_e role; //enum

} user_t;

user_t *user_create(
    char *username,
    char *email,
    mobwa_payments_hub_user_ROLE_e role
);

void user_free(user_t *user);

user_t *user_parseFromJSON(cJSON *userJSON);

cJSON *user_convertToJSON(user_t *user);

#endif /* _user_H_ */

