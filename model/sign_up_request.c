#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sign_up_request.h"



sign_up_request_t *sign_up_request_create(
    char *username,
    char *email,
    char *password
    ) {
    sign_up_request_t *sign_up_request_local_var = malloc(sizeof(sign_up_request_t));
    if (!sign_up_request_local_var) {
        return NULL;
    }
    sign_up_request_local_var->username = username;
    sign_up_request_local_var->email = email;
    sign_up_request_local_var->password = password;

    return sign_up_request_local_var;
}


void sign_up_request_free(sign_up_request_t *sign_up_request) {
    if(NULL == sign_up_request){
        return ;
    }
    listEntry_t *listEntry;
    if (sign_up_request->username) {
        free(sign_up_request->username);
        sign_up_request->username = NULL;
    }
    if (sign_up_request->email) {
        free(sign_up_request->email);
        sign_up_request->email = NULL;
    }
    if (sign_up_request->password) {
        free(sign_up_request->password);
        sign_up_request->password = NULL;
    }
    free(sign_up_request);
}

cJSON *sign_up_request_convertToJSON(sign_up_request_t *sign_up_request) {
    cJSON *item = cJSON_CreateObject();

    // sign_up_request->username
    if(sign_up_request->username) { 
    if(cJSON_AddStringToObject(item, "username", sign_up_request->username) == NULL) {
    goto fail; //String
    }
     } 


    // sign_up_request->email
    if(sign_up_request->email) { 
    if(cJSON_AddStringToObject(item, "email", sign_up_request->email) == NULL) {
    goto fail; //String
    }
     } 


    // sign_up_request->password
    if(sign_up_request->password) { 
    if(cJSON_AddStringToObject(item, "password", sign_up_request->password) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

sign_up_request_t *sign_up_request_parseFromJSON(cJSON *sign_up_requestJSON){

    sign_up_request_t *sign_up_request_local_var = NULL;

    // sign_up_request->username
    cJSON *username = cJSON_GetObjectItemCaseSensitive(sign_up_requestJSON, "username");
    if (username) { 
    if(!cJSON_IsString(username))
    {
    goto end; //String
    }
    }

    // sign_up_request->email
    cJSON *email = cJSON_GetObjectItemCaseSensitive(sign_up_requestJSON, "email");
    if (email) { 
    if(!cJSON_IsString(email))
    {
    goto end; //String
    }
    }

    // sign_up_request->password
    cJSON *password = cJSON_GetObjectItemCaseSensitive(sign_up_requestJSON, "password");
    if (password) { 
    if(!cJSON_IsString(password))
    {
    goto end; //String
    }
    }


    sign_up_request_local_var = sign_up_request_create (
        username ? strdup(username->valuestring) : NULL,
        email ? strdup(email->valuestring) : NULL,
        password ? strdup(password->valuestring) : NULL
        );

    return sign_up_request_local_var;
end:
    return NULL;

}
