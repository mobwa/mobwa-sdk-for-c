#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "update_account_request.h"



update_account_request_t *update_account_request_create(
    char *name
    ) {
    update_account_request_t *update_account_request_local_var = malloc(sizeof(update_account_request_t));
    if (!update_account_request_local_var) {
        return NULL;
    }
    update_account_request_local_var->name = name;

    return update_account_request_local_var;
}


void update_account_request_free(update_account_request_t *update_account_request) {
    if(NULL == update_account_request){
        return ;
    }
    listEntry_t *listEntry;
    if (update_account_request->name) {
        free(update_account_request->name);
        update_account_request->name = NULL;
    }
    free(update_account_request);
}

cJSON *update_account_request_convertToJSON(update_account_request_t *update_account_request) {
    cJSON *item = cJSON_CreateObject();

    // update_account_request->name
    if(update_account_request->name) { 
    if(cJSON_AddStringToObject(item, "name", update_account_request->name) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

update_account_request_t *update_account_request_parseFromJSON(cJSON *update_account_requestJSON){

    update_account_request_t *update_account_request_local_var = NULL;

    // update_account_request->name
    cJSON *name = cJSON_GetObjectItemCaseSensitive(update_account_requestJSON, "name");
    if (name) { 
    if(!cJSON_IsString(name))
    {
    goto end; //String
    }
    }


    update_account_request_local_var = update_account_request_create (
        name ? strdup(name->valuestring) : NULL
        );

    return update_account_request_local_var;
end:
    return NULL;

}
