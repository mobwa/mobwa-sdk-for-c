/*
 * account.h
 *
 * 
 */

#ifndef _account_H_
#define _account_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct account_t account_t;


// Enum CURRENCY for account

typedef enum  { mobwa_payments_hub_account_CURRENCY_NULL = 0, mobwa_payments_hub_account_CURRENCY_USD, mobwa_payments_hub_account_CURRENCY_SGD } mobwa_payments_hub_account_CURRENCY_e;

char* account_currency_ToString(mobwa_payments_hub_account_CURRENCY_e currency);

mobwa_payments_hub_account_CURRENCY_e account_currency_FromString(char* currency);



typedef struct account_t {
    char *id; // string
    char *name; // string
    char *created_at; // string
    mobwa_payments_hub_account_CURRENCY_e currency; //enum
    char *updated_at; // string
    int balance; //numeric

} account_t;

account_t *account_create(
    char *id,
    char *name,
    char *created_at,
    mobwa_payments_hub_account_CURRENCY_e currency,
    char *updated_at,
    int balance
);

void account_free(account_t *account);

account_t *account_parseFromJSON(cJSON *accountJSON);

cJSON *account_convertToJSON(account_t *account);

#endif /* _account_H_ */

