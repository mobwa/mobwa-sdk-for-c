/*
 * create_transfer_request.h
 *
 * 
 */

#ifndef _create_transfer_request_H_
#define _create_transfer_request_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct create_transfer_request_t create_transfer_request_t;


// Enum CURRENCY for create_transfer_request

typedef enum  { mobwa_payments_hub_create_transfer_request_CURRENCY_NULL = 0, mobwa_payments_hub_create_transfer_request_CURRENCY_SGD, mobwa_payments_hub_create_transfer_request_CURRENCY_USD } mobwa_payments_hub_create_transfer_request_CURRENCY_e;

char* create_transfer_request_currency_ToString(mobwa_payments_hub_create_transfer_request_CURRENCY_e currency);

mobwa_payments_hub_create_transfer_request_CURRENCY_e create_transfer_request_currency_FromString(char* currency);



typedef struct create_transfer_request_t {
    char *message; // string
    int amount; //numeric
    mobwa_payments_hub_create_transfer_request_CURRENCY_e currency; //enum
    int auto_complete; //boolean
    char *destination; // string

} create_transfer_request_t;

create_transfer_request_t *create_transfer_request_create(
    char *message,
    int amount,
    mobwa_payments_hub_create_transfer_request_CURRENCY_e currency,
    int auto_complete,
    char *destination
);

void create_transfer_request_free(create_transfer_request_t *create_transfer_request);

create_transfer_request_t *create_transfer_request_parseFromJSON(cJSON *create_transfer_requestJSON);

cJSON *create_transfer_request_convertToJSON(create_transfer_request_t *create_transfer_request);

#endif /* _create_transfer_request_H_ */

