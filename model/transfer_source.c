#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "transfer_source.h"



transfer_source_t *transfer_source_create(
    char *account_id
    ) {
    transfer_source_t *transfer_source_local_var = malloc(sizeof(transfer_source_t));
    if (!transfer_source_local_var) {
        return NULL;
    }
    transfer_source_local_var->account_id = account_id;

    return transfer_source_local_var;
}


void transfer_source_free(transfer_source_t *transfer_source) {
    if(NULL == transfer_source){
        return ;
    }
    listEntry_t *listEntry;
    if (transfer_source->account_id) {
        free(transfer_source->account_id);
        transfer_source->account_id = NULL;
    }
    free(transfer_source);
}

cJSON *transfer_source_convertToJSON(transfer_source_t *transfer_source) {
    cJSON *item = cJSON_CreateObject();

    // transfer_source->account_id
    if(transfer_source->account_id) { 
    if(cJSON_AddStringToObject(item, "account_id", transfer_source->account_id) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

transfer_source_t *transfer_source_parseFromJSON(cJSON *transfer_sourceJSON){

    transfer_source_t *transfer_source_local_var = NULL;

    // transfer_source->account_id
    cJSON *account_id = cJSON_GetObjectItemCaseSensitive(transfer_sourceJSON, "account_id");
    if (account_id) { 
    if(!cJSON_IsString(account_id))
    {
    goto end; //String
    }
    }


    transfer_source_local_var = transfer_source_create (
        account_id ? strdup(account_id->valuestring) : NULL
        );

    return transfer_source_local_var;
end:
    return NULL;

}
