/*
 * recharge_account_request.h
 *
 * 
 */

#ifndef _recharge_account_request_H_
#define _recharge_account_request_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct recharge_account_request_t recharge_account_request_t;




typedef struct recharge_account_request_t {
    int amout; //numeric

} recharge_account_request_t;

recharge_account_request_t *recharge_account_request_create(
    int amout
);

void recharge_account_request_free(recharge_account_request_t *recharge_account_request);

recharge_account_request_t *recharge_account_request_parseFromJSON(cJSON *recharge_account_requestJSON);

cJSON *recharge_account_request_convertToJSON(recharge_account_request_t *recharge_account_request);

#endif /* _recharge_account_request_H_ */

