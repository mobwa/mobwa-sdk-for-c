/*
 * create_user_request.h
 *
 * 
 */

#ifndef _create_user_request_H_
#define _create_user_request_H_

#include <string.h>
#include "../external/cJSON.h"
#include "../include/list.h"
#include "../include/keyValuePair.h"
#include "../include/binary.h"

typedef struct create_user_request_t create_user_request_t;


// Enum ROLE for create_user_request

typedef enum  { mobwa_payments_hub_create_user_request_ROLE_NULL = 0, mobwa_payments_hub_create_user_request_ROLE_CUSTOMER, mobwa_payments_hub_create_user_request_ROLE_ADMIN } mobwa_payments_hub_create_user_request_ROLE_e;

char* create_user_request_role_ToString(mobwa_payments_hub_create_user_request_ROLE_e role);

mobwa_payments_hub_create_user_request_ROLE_e create_user_request_role_FromString(char* role);



typedef struct create_user_request_t {
    char *username; // string
    char *email; // string
    char *password; // string
    mobwa_payments_hub_create_user_request_ROLE_e role; //enum

} create_user_request_t;

create_user_request_t *create_user_request_create(
    char *username,
    char *email,
    char *password,
    mobwa_payments_hub_create_user_request_ROLE_e role
);

void create_user_request_free(create_user_request_t *create_user_request);

create_user_request_t *create_user_request_parseFromJSON(cJSON *create_user_requestJSON);

cJSON *create_user_request_convertToJSON(create_user_request_t *create_user_request);

#endif /* _create_user_request_H_ */

