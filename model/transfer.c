#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "transfer.h"


char* currencytransfer_ToString(mobwa_payments_hub_transfer_CURRENCY_e currency) {
    char* currencyArray[] =  { "NULL", "SGD", "USD" };
	return currencyArray[currency];
}

mobwa_payments_hub_transfer_CURRENCY_e currencytransfer_FromString(char* currency){
    int stringToReturn = 0;
    char *currencyArray[] =  { "NULL", "SGD", "USD" };
    size_t sizeofArray = sizeof(currencyArray) / sizeof(currencyArray[0]);
    while(stringToReturn < sizeofArray) {
        if(strcmp(currency, currencyArray[stringToReturn]) == 0) {
            return stringToReturn;
        }
        stringToReturn++;
    }
    return 0;
}
char* statustransfer_ToString(mobwa_payments_hub_transfer_STATUS_e status) {
    char* statusArray[] =  { "NULL", "PENDING", "SUCCEEDED", "FAILED" };
	return statusArray[status];
}

mobwa_payments_hub_transfer_STATUS_e statustransfer_FromString(char* status){
    int stringToReturn = 0;
    char *statusArray[] =  { "NULL", "PENDING", "SUCCEEDED", "FAILED" };
    size_t sizeofArray = sizeof(statusArray) / sizeof(statusArray[0]);
    while(stringToReturn < sizeofArray) {
        if(strcmp(status, statusArray[stringToReturn]) == 0) {
            return stringToReturn;
        }
        stringToReturn++;
    }
    return 0;
}

transfer_t *transfer_create(
    char *id,
    char *message,
    int amount,
    mobwa_payments_hub_transfer_CURRENCY_e currency,
    char *created_at,
    char *updated_at,
    mobwa_payments_hub_transfer_STATUS_e status,
    int auto_complete,
    transfer_source_t *source,
    transfer_source_t *destination
    ) {
    transfer_t *transfer_local_var = malloc(sizeof(transfer_t));
    if (!transfer_local_var) {
        return NULL;
    }
    transfer_local_var->id = id;
    transfer_local_var->message = message;
    transfer_local_var->amount = amount;
    transfer_local_var->currency = currency;
    transfer_local_var->created_at = created_at;
    transfer_local_var->updated_at = updated_at;
    transfer_local_var->status = status;
    transfer_local_var->auto_complete = auto_complete;
    transfer_local_var->source = source;
    transfer_local_var->destination = destination;

    return transfer_local_var;
}


void transfer_free(transfer_t *transfer) {
    if(NULL == transfer){
        return ;
    }
    listEntry_t *listEntry;
    if (transfer->id) {
        free(transfer->id);
        transfer->id = NULL;
    }
    if (transfer->message) {
        free(transfer->message);
        transfer->message = NULL;
    }
    if (transfer->created_at) {
        free(transfer->created_at);
        transfer->created_at = NULL;
    }
    if (transfer->updated_at) {
        free(transfer->updated_at);
        transfer->updated_at = NULL;
    }
    if (transfer->source) {
        transfer_source_free(transfer->source);
        transfer->source = NULL;
    }
    if (transfer->destination) {
        transfer_source_free(transfer->destination);
        transfer->destination = NULL;
    }
    free(transfer);
}

cJSON *transfer_convertToJSON(transfer_t *transfer) {
    cJSON *item = cJSON_CreateObject();

    // transfer->id
    if(transfer->id) { 
    if(cJSON_AddStringToObject(item, "id", transfer->id) == NULL) {
    goto fail; //String
    }
     } 


    // transfer->message
    if(transfer->message) { 
    if(cJSON_AddStringToObject(item, "message", transfer->message) == NULL) {
    goto fail; //String
    }
     } 


    // transfer->amount
    if(transfer->amount) { 
    if(cJSON_AddNumberToObject(item, "amount", transfer->amount) == NULL) {
    goto fail; //Numeric
    }
     } 


    // transfer->currency
    
    if(cJSON_AddStringToObject(item, "currency", currencytransfer_ToString(transfer->currency)) == NULL)
    {
    goto fail; //Enum
    }
    


    // transfer->created_at
    if(transfer->created_at) { 
    if(cJSON_AddStringToObject(item, "created_at", transfer->created_at) == NULL) {
    goto fail; //String
    }
     } 


    // transfer->updated_at
    if(transfer->updated_at) { 
    if(cJSON_AddStringToObject(item, "updated_at", transfer->updated_at) == NULL) {
    goto fail; //String
    }
     } 


    // transfer->status
    
    if(cJSON_AddStringToObject(item, "status", statustransfer_ToString(transfer->status)) == NULL)
    {
    goto fail; //Enum
    }
    


    // transfer->auto_complete
    if(transfer->auto_complete) { 
    if(cJSON_AddBoolToObject(item, "auto_complete", transfer->auto_complete) == NULL) {
    goto fail; //Bool
    }
     } 


    // transfer->source
    if(transfer->source) { 
    cJSON *source_local_JSON = transfer_source_convertToJSON(transfer->source);
    if(source_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "source", source_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 


    // transfer->destination
    if(transfer->destination) { 
    cJSON *destination_local_JSON = transfer_source_convertToJSON(transfer->destination);
    if(destination_local_JSON == NULL) {
    goto fail; //model
    }
    cJSON_AddItemToObject(item, "destination", destination_local_JSON);
    if(item->child == NULL) {
    goto fail;
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

transfer_t *transfer_parseFromJSON(cJSON *transferJSON){

    transfer_t *transfer_local_var = NULL;

    // transfer->id
    cJSON *id = cJSON_GetObjectItemCaseSensitive(transferJSON, "id");
    if (id) { 
    if(!cJSON_IsString(id))
    {
    goto end; //String
    }
    }

    // transfer->message
    cJSON *message = cJSON_GetObjectItemCaseSensitive(transferJSON, "message");
    if (message) { 
    if(!cJSON_IsString(message))
    {
    goto end; //String
    }
    }

    // transfer->amount
    cJSON *amount = cJSON_GetObjectItemCaseSensitive(transferJSON, "amount");
    if (amount) { 
    if(!cJSON_IsNumber(amount))
    {
    goto end; //Numeric
    }
    }

    // transfer->currency
    cJSON *currency = cJSON_GetObjectItemCaseSensitive(transferJSON, "currency");
    mobwa_payments_hub_transfer_CURRENCY_e currencyVariable;
    if (currency) { 
    if(!cJSON_IsString(currency))
    {
    goto end; //Enum
    }
    currencyVariable = currencytransfer_FromString(currency->valuestring);
    }

    // transfer->created_at
    cJSON *created_at = cJSON_GetObjectItemCaseSensitive(transferJSON, "created_at");
    if (created_at) { 
    if(!cJSON_IsString(created_at))
    {
    goto end; //String
    }
    }

    // transfer->updated_at
    cJSON *updated_at = cJSON_GetObjectItemCaseSensitive(transferJSON, "updated_at");
    if (updated_at) { 
    if(!cJSON_IsString(updated_at))
    {
    goto end; //String
    }
    }

    // transfer->status
    cJSON *status = cJSON_GetObjectItemCaseSensitive(transferJSON, "status");
    mobwa_payments_hub_transfer_STATUS_e statusVariable;
    if (status) { 
    if(!cJSON_IsString(status))
    {
    goto end; //Enum
    }
    statusVariable = statustransfer_FromString(status->valuestring);
    }

    // transfer->auto_complete
    cJSON *auto_complete = cJSON_GetObjectItemCaseSensitive(transferJSON, "auto_complete");
    if (auto_complete) { 
    if(!cJSON_IsBool(auto_complete))
    {
    goto end; //Bool
    }
    }

    // transfer->source
    cJSON *source = cJSON_GetObjectItemCaseSensitive(transferJSON, "source");
    transfer_source_t *source_local_nonprim = NULL;
    if (source) { 
    source_local_nonprim = transfer_source_parseFromJSON(source); //nonprimitive
    }

    // transfer->destination
    cJSON *destination = cJSON_GetObjectItemCaseSensitive(transferJSON, "destination");
    transfer_source_t *destination_local_nonprim = NULL;
    if (destination) { 
    destination_local_nonprim = transfer_source_parseFromJSON(destination); //nonprimitive
    }


    transfer_local_var = transfer_create (
        id ? strdup(id->valuestring) : NULL,
        message ? strdup(message->valuestring) : NULL,
        amount ? amount->valuedouble : 0,
        currency ? currencyVariable : -1,
        created_at ? strdup(created_at->valuestring) : NULL,
        updated_at ? strdup(updated_at->valuestring) : NULL,
        status ? statusVariable : -1,
        auto_complete ? auto_complete->valueint : 0,
        source ? source_local_nonprim : NULL,
        destination ? destination_local_nonprim : NULL
        );

    return transfer_local_var;
end:
    if (source_local_nonprim) {
        transfer_source_free(source_local_nonprim);
        source_local_nonprim = NULL;
    }
    if (destination_local_nonprim) {
        transfer_source_free(destination_local_nonprim);
        destination_local_nonprim = NULL;
    }
    return NULL;

}
