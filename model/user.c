#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "user.h"


char* roleuser_ToString(mobwa_payments_hub_user_ROLE_e role) {
    char* roleArray[] =  { "NULL", "CUSTOMER", "ADMIN" };
	return roleArray[role];
}

mobwa_payments_hub_user_ROLE_e roleuser_FromString(char* role){
    int stringToReturn = 0;
    char *roleArray[] =  { "NULL", "CUSTOMER", "ADMIN" };
    size_t sizeofArray = sizeof(roleArray) / sizeof(roleArray[0]);
    while(stringToReturn < sizeofArray) {
        if(strcmp(role, roleArray[stringToReturn]) == 0) {
            return stringToReturn;
        }
        stringToReturn++;
    }
    return 0;
}

user_t *user_create(
    char *username,
    char *email,
    mobwa_payments_hub_user_ROLE_e role
    ) {
    user_t *user_local_var = malloc(sizeof(user_t));
    if (!user_local_var) {
        return NULL;
    }
    user_local_var->username = username;
    user_local_var->email = email;
    user_local_var->role = role;

    return user_local_var;
}


void user_free(user_t *user) {
    if(NULL == user){
        return ;
    }
    listEntry_t *listEntry;
    if (user->username) {
        free(user->username);
        user->username = NULL;
    }
    if (user->email) {
        free(user->email);
        user->email = NULL;
    }
    free(user);
}

cJSON *user_convertToJSON(user_t *user) {
    cJSON *item = cJSON_CreateObject();

    // user->username
    if(user->username) { 
    if(cJSON_AddStringToObject(item, "username", user->username) == NULL) {
    goto fail; //String
    }
     } 


    // user->email
    if(user->email) { 
    if(cJSON_AddStringToObject(item, "email", user->email) == NULL) {
    goto fail; //String
    }
     } 


    // user->role
    
    if(cJSON_AddStringToObject(item, "role", roleuser_ToString(user->role)) == NULL)
    {
    goto fail; //Enum
    }
    

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

user_t *user_parseFromJSON(cJSON *userJSON){

    user_t *user_local_var = NULL;

    // user->username
    cJSON *username = cJSON_GetObjectItemCaseSensitive(userJSON, "username");
    if (username) { 
    if(!cJSON_IsString(username))
    {
    goto end; //String
    }
    }

    // user->email
    cJSON *email = cJSON_GetObjectItemCaseSensitive(userJSON, "email");
    if (email) { 
    if(!cJSON_IsString(email))
    {
    goto end; //String
    }
    }

    // user->role
    cJSON *role = cJSON_GetObjectItemCaseSensitive(userJSON, "role");
    mobwa_payments_hub_user_ROLE_e roleVariable;
    if (role) { 
    if(!cJSON_IsString(role))
    {
    goto end; //Enum
    }
    roleVariable = roleuser_FromString(role->valuestring);
    }


    user_local_var = user_create (
        username ? strdup(username->valuestring) : NULL,
        email ? strdup(email->valuestring) : NULL,
        role ? roleVariable : -1
        );

    return user_local_var;
end:
    return NULL;

}
