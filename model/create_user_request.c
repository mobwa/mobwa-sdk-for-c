#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "create_user_request.h"


char* rolecreate_user_request_ToString(mobwa_payments_hub_create_user_request_ROLE_e role) {
    char* roleArray[] =  { "NULL", "CUSTOMER", "ADMIN" };
	return roleArray[role];
}

mobwa_payments_hub_create_user_request_ROLE_e rolecreate_user_request_FromString(char* role){
    int stringToReturn = 0;
    char *roleArray[] =  { "NULL", "CUSTOMER", "ADMIN" };
    size_t sizeofArray = sizeof(roleArray) / sizeof(roleArray[0]);
    while(stringToReturn < sizeofArray) {
        if(strcmp(role, roleArray[stringToReturn]) == 0) {
            return stringToReturn;
        }
        stringToReturn++;
    }
    return 0;
}

create_user_request_t *create_user_request_create(
    char *username,
    char *email,
    char *password,
    mobwa_payments_hub_create_user_request_ROLE_e role
    ) {
    create_user_request_t *create_user_request_local_var = malloc(sizeof(create_user_request_t));
    if (!create_user_request_local_var) {
        return NULL;
    }
    create_user_request_local_var->username = username;
    create_user_request_local_var->email = email;
    create_user_request_local_var->password = password;
    create_user_request_local_var->role = role;

    return create_user_request_local_var;
}


void create_user_request_free(create_user_request_t *create_user_request) {
    if(NULL == create_user_request){
        return ;
    }
    listEntry_t *listEntry;
    if (create_user_request->username) {
        free(create_user_request->username);
        create_user_request->username = NULL;
    }
    if (create_user_request->email) {
        free(create_user_request->email);
        create_user_request->email = NULL;
    }
    if (create_user_request->password) {
        free(create_user_request->password);
        create_user_request->password = NULL;
    }
    free(create_user_request);
}

cJSON *create_user_request_convertToJSON(create_user_request_t *create_user_request) {
    cJSON *item = cJSON_CreateObject();

    // create_user_request->username
    if(create_user_request->username) { 
    if(cJSON_AddStringToObject(item, "username", create_user_request->username) == NULL) {
    goto fail; //String
    }
     } 


    // create_user_request->email
    if(create_user_request->email) { 
    if(cJSON_AddStringToObject(item, "email", create_user_request->email) == NULL) {
    goto fail; //String
    }
     } 


    // create_user_request->password
    if(create_user_request->password) { 
    if(cJSON_AddStringToObject(item, "password", create_user_request->password) == NULL) {
    goto fail; //String
    }
     } 


    // create_user_request->role
    
    if(cJSON_AddStringToObject(item, "role", rolecreate_user_request_ToString(create_user_request->role)) == NULL)
    {
    goto fail; //Enum
    }
    

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

create_user_request_t *create_user_request_parseFromJSON(cJSON *create_user_requestJSON){

    create_user_request_t *create_user_request_local_var = NULL;

    // create_user_request->username
    cJSON *username = cJSON_GetObjectItemCaseSensitive(create_user_requestJSON, "username");
    if (username) { 
    if(!cJSON_IsString(username))
    {
    goto end; //String
    }
    }

    // create_user_request->email
    cJSON *email = cJSON_GetObjectItemCaseSensitive(create_user_requestJSON, "email");
    if (email) { 
    if(!cJSON_IsString(email))
    {
    goto end; //String
    }
    }

    // create_user_request->password
    cJSON *password = cJSON_GetObjectItemCaseSensitive(create_user_requestJSON, "password");
    if (password) { 
    if(!cJSON_IsString(password))
    {
    goto end; //String
    }
    }

    // create_user_request->role
    cJSON *role = cJSON_GetObjectItemCaseSensitive(create_user_requestJSON, "role");
    mobwa_payments_hub_create_user_request_ROLE_e roleVariable;
    if (role) { 
    if(!cJSON_IsString(role))
    {
    goto end; //Enum
    }
    roleVariable = rolecreate_user_request_FromString(role->valuestring);
    }


    create_user_request_local_var = create_user_request_create (
        username ? strdup(username->valuestring) : NULL,
        email ? strdup(email->valuestring) : NULL,
        password ? strdup(password->valuestring) : NULL,
        role ? roleVariable : -1
        );

    return create_user_request_local_var;
end:
    return NULL;

}
