#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "create_transfer_request.h"


char* currencycreate_transfer_request_ToString(mobwa_payments_hub_create_transfer_request_CURRENCY_e currency) {
    char* currencyArray[] =  { "NULL", "SGD", "USD" };
	return currencyArray[currency];
}

mobwa_payments_hub_create_transfer_request_CURRENCY_e currencycreate_transfer_request_FromString(char* currency){
    int stringToReturn = 0;
    char *currencyArray[] =  { "NULL", "SGD", "USD" };
    size_t sizeofArray = sizeof(currencyArray) / sizeof(currencyArray[0]);
    while(stringToReturn < sizeofArray) {
        if(strcmp(currency, currencyArray[stringToReturn]) == 0) {
            return stringToReturn;
        }
        stringToReturn++;
    }
    return 0;
}

create_transfer_request_t *create_transfer_request_create(
    char *message,
    int amount,
    mobwa_payments_hub_create_transfer_request_CURRENCY_e currency,
    int auto_complete,
    char *destination
    ) {
    create_transfer_request_t *create_transfer_request_local_var = malloc(sizeof(create_transfer_request_t));
    if (!create_transfer_request_local_var) {
        return NULL;
    }
    create_transfer_request_local_var->message = message;
    create_transfer_request_local_var->amount = amount;
    create_transfer_request_local_var->currency = currency;
    create_transfer_request_local_var->auto_complete = auto_complete;
    create_transfer_request_local_var->destination = destination;

    return create_transfer_request_local_var;
}


void create_transfer_request_free(create_transfer_request_t *create_transfer_request) {
    if(NULL == create_transfer_request){
        return ;
    }
    listEntry_t *listEntry;
    if (create_transfer_request->message) {
        free(create_transfer_request->message);
        create_transfer_request->message = NULL;
    }
    if (create_transfer_request->destination) {
        free(create_transfer_request->destination);
        create_transfer_request->destination = NULL;
    }
    free(create_transfer_request);
}

cJSON *create_transfer_request_convertToJSON(create_transfer_request_t *create_transfer_request) {
    cJSON *item = cJSON_CreateObject();

    // create_transfer_request->message
    if(create_transfer_request->message) { 
    if(cJSON_AddStringToObject(item, "message", create_transfer_request->message) == NULL) {
    goto fail; //String
    }
     } 


    // create_transfer_request->amount
    if(create_transfer_request->amount) { 
    if(cJSON_AddNumberToObject(item, "amount", create_transfer_request->amount) == NULL) {
    goto fail; //Numeric
    }
     } 


    // create_transfer_request->currency
    
    if(cJSON_AddStringToObject(item, "currency", currencycreate_transfer_request_ToString(create_transfer_request->currency)) == NULL)
    {
    goto fail; //Enum
    }
    


    // create_transfer_request->auto_complete
    if(create_transfer_request->auto_complete) { 
    if(cJSON_AddBoolToObject(item, "auto_complete", create_transfer_request->auto_complete) == NULL) {
    goto fail; //Bool
    }
     } 


    // create_transfer_request->destination
    if(create_transfer_request->destination) { 
    if(cJSON_AddStringToObject(item, "destination", create_transfer_request->destination) == NULL) {
    goto fail; //String
    }
     } 

    return item;
fail:
    if (item) {
        cJSON_Delete(item);
    }
    return NULL;
}

create_transfer_request_t *create_transfer_request_parseFromJSON(cJSON *create_transfer_requestJSON){

    create_transfer_request_t *create_transfer_request_local_var = NULL;

    // create_transfer_request->message
    cJSON *message = cJSON_GetObjectItemCaseSensitive(create_transfer_requestJSON, "message");
    if (message) { 
    if(!cJSON_IsString(message))
    {
    goto end; //String
    }
    }

    // create_transfer_request->amount
    cJSON *amount = cJSON_GetObjectItemCaseSensitive(create_transfer_requestJSON, "amount");
    if (amount) { 
    if(!cJSON_IsNumber(amount))
    {
    goto end; //Numeric
    }
    }

    // create_transfer_request->currency
    cJSON *currency = cJSON_GetObjectItemCaseSensitive(create_transfer_requestJSON, "currency");
    mobwa_payments_hub_create_transfer_request_CURRENCY_e currencyVariable;
    if (currency) { 
    if(!cJSON_IsString(currency))
    {
    goto end; //Enum
    }
    currencyVariable = currencycreate_transfer_request_FromString(currency->valuestring);
    }

    // create_transfer_request->auto_complete
    cJSON *auto_complete = cJSON_GetObjectItemCaseSensitive(create_transfer_requestJSON, "auto_complete");
    if (auto_complete) { 
    if(!cJSON_IsBool(auto_complete))
    {
    goto end; //Bool
    }
    }

    // create_transfer_request->destination
    cJSON *destination = cJSON_GetObjectItemCaseSensitive(create_transfer_requestJSON, "destination");
    if (destination) { 
    if(!cJSON_IsString(destination))
    {
    goto end; //String
    }
    }


    create_transfer_request_local_var = create_transfer_request_create (
        message ? strdup(message->valuestring) : NULL,
        amount ? amount->valuedouble : 0,
        currency ? currencyVariable : -1,
        auto_complete ? auto_complete->valueint : 0,
        destination ? strdup(destination->valuestring) : NULL
        );

    return create_transfer_request_local_var;
end:
    return NULL;

}
